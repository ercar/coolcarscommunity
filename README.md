Cool Cars Community webpage
==========================

This is a webpage about toy-cars made for a school assignment.
Techniques that I've used are Spring MVC, mysql database with JDBC and JPA API,
HTML/thymeleaf and a little bit of javascript and bootstrap framework.
Multilayered architecture with layers: presentation, logic and persistence.


Features
--------------------

A selection of features at this webpage: 

- Page acess secured with spring security.
- To access carfactory.html you have to login.
Username and password are stored in database in member table.
- At carfactory.html you can add a new car with info and picture.
- index.html includes a bootstrap carousel that shows top five cars with highest score.
- index.html shows the last three newly added cars.
- carlist.html shows all cars.
- cars have car profiles.



How to use - instructions
--------------------

1. Import to Eclipse as a maven project.

2. Currently you have to change the code at method addResourceHandlers
in Application.java and update location where you want to store car images and member-avatars images.

3. Build the database from cars_db.sql located under config/.  

4. Run class Application.java to start the server.