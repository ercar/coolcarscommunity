package se.eric.coolcarscommunity.logic.domainobjects;

import java.util.Date;

public class MemberDO {

private Date lastLogin;
private String memberActive;
private String memberAvatar;
private String memberIp;
private String memberLevel;
private String memberName;
private String memberPassword;
private String memberMail;
private String memberAbout;


public MemberDO() {
	
}


public MemberDO(Date lastLogin, String memberActive, String memberAvatar,
		String memberIp, String memberLevel, String memberName,
		String memberPassword, String memberMail, String memberAbout) {
	super();
	this.lastLogin = lastLogin;
	this.memberActive = memberActive;
	this.memberAvatar = memberAvatar;
	this.memberIp = memberIp;
	this.memberLevel = memberLevel;
	this.memberName = memberName;
	this.memberPassword = memberPassword;
	this.memberMail = memberMail;
	this.memberAbout = memberAbout;
}


public Date getLastLogin() {
	return lastLogin;
}


public void setLastLogin(Date lastLogin) {
	this.lastLogin = lastLogin;
}


public String getMemberActive() {
	return memberActive;
}


public void setMemberActive(String memberActive) {
	this.memberActive = memberActive;
}


public String getMemberAvatar() {
	return memberAvatar;
}


public void setMemberAvatar(String memberAvatar) {
	this.memberAvatar = memberAvatar;
}


public String getMemberIp() {
	return memberIp;
}


public void setMemberIp(String memberIp) {
	this.memberIp = memberIp;
}


public String getMemberLevel() {
	return memberLevel;
}


public void setMemberLevel(String memberLevel) {
	this.memberLevel = memberLevel;
}


public String getMemberName() {
	return memberName;
}


public void setMemberName(String memberName) {
	this.memberName = memberName;
}


public String getMemberPassword() {
	return memberPassword;
}


public void setMemberPassword(String memberPassword) {
	this.memberPassword = memberPassword;
}


public String getMemberMail() {
	return memberMail;
}


public void setMemberMail(String memberMail) {
	this.memberMail = memberMail;
}


public String getMemberAbout() {
	return memberAbout;
}


public void setMemberAbout(String memberAbout) {
	this.memberAbout = memberAbout;
}
	

}
