package se.eric.coolcarscommunity.logic.domainobjects;

import se.eric.coolcarscommunity.persistence.database.MemberEntity;

public class NewsDO {

	private String newsActive;
	private String newsContent;
	private String newsType;
	private MemberEntity member;

	public NewsDO() {

	}

	public NewsDO(String newsActive, String newsContent, String newsType,
			MemberEntity member) {
		super();
		this.newsActive = newsActive;
		this.newsContent = newsContent;
		this.newsType = newsType;
		this.member = member;
	}

	public String getNewsActive() {
		return newsActive;
	}

	public void setNewsActive(String newsActive) {
		this.newsActive = newsActive;
	}

	public String getNewsContent() {
		return newsContent;
	}

	public void setNewsContent(String newsContent) {
		this.newsContent = newsContent;
	}

	public String getNewsType() {
		return newsType;
	}

	public void setNewsType(String newsType) {
		this.newsType = newsType;
	}

	public MemberEntity getMember() {
		return member;
	}

	public void setMember(MemberEntity member) {
		this.member = member;
	}

}
