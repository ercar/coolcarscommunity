package se.eric.coolcarscommunity.logic.domainobjects;

import java.util.Date;

import se.eric.coolcarscommunity.persistence.database.MemberEntity;

public class CarDO {

	private int pKey;
	private String carName;
	private String carImage;
	private String carBrand;
	private String carColor;
	private String carType;
	private String carDescription;
	private int carRating;
	private int carVotes;
	private Date createDate;
	private MemberEntity member;

	public CarDO() {

	}

	public CarDO(int pKey, String carName, String carImage, String carBrand,
			String carColor, String carType, String carDescription,
			int carRating, int carVotes, Date createDate, MemberEntity member) {
		super();
		this.pKey = pKey;
		this.carName = carName;
		this.carImage = carImage;
		this.carBrand = carBrand;
		this.carColor = carColor;
		this.carType = carType;
		this.carDescription = carDescription;
		this.carRating = carRating;
		this.carVotes = carVotes;
		this.createDate = createDate;
		this.member = member;
	}

	public int getpKey() {
		return pKey;
	}

	public void setpKey(int pKey) {
		this.pKey = pKey;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getCarImage() {
		return carImage;
	}

	public void setCarImage(String carImage) {
		this.carImage = carImage;
	}

	public String getCarBrand() {
		return carBrand;
	}

	public void setCarBrand(String carBrand) {
		this.carBrand = carBrand;
	}

	public String getCarColor() {
		return carColor;
	}

	public void setCarColor(String carColor) {
		this.carColor = carColor;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getCarDescription() {
		return carDescription;
	}

	public void setCarDescription(String carDescription) {
		this.carDescription = carDescription;
	}

	public int getCarRating() {
		return carRating;
	}

	public void setCarRating(int carRating) {
		this.carRating = carRating;
	}

	public int getCarVotes() {
		return carVotes;
	}

	public void setCarVotes(int carVotes) {
		this.carVotes = carVotes;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public MemberEntity getMember() {
		return member;
	}

	public void setMember(MemberEntity member) {
		this.member = member;
	}

}
