package se.eric.coolcarscommunity.logic;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import se.eric.coolcarscommunity.logic.domainobjects.CarDO;
import se.eric.coolcarscommunity.persistence.PersistenceManager;
import se.eric.coolcarscommunity.persistence.database.MemberEntity;

@Component
public class CarHandlingService {

	@Autowired
	PersistenceManager persistenceManager;

	List<CarDO> cars = new ArrayList<CarDO>();
	List<CarDO> topFiveCars = new ArrayList<CarDO>();
	List<CarDO> newCars = new ArrayList<CarDO>();

	public CarHandlingService() {

	}

	@PostConstruct
	public void initIt() throws Exception {
		
		createTopFiveCars();
		System.out.println("createTopFiveCars kallad!");
		createNewCars();
		System.out.println("createNewCars kallad!");

	}

	public List<CarDO> getAllCars() {

		final List<CarDO> cars = persistenceManager.getAllCars();
		return cars;

	}

	public void SaveCar(CarDO carDO) {

		persistenceManager.SaveCarEntity(carDO);

	}

	public void createTopFiveCars() {

		this.topFiveCars.clear();
		this.topFiveCars.addAll(persistenceManager.getTopFiveCars());

	}

	public void createNewCars() {

		this.newCars.clear();
		this.newCars.addAll(persistenceManager.getNewCars());

	}

	public List<CarDO> getTopFiveCars() {
		return topFiveCars;
	}

	public void setTopFiveCars(List<CarDO> topFiveCars) {
		this.topFiveCars = topFiveCars;
	}

	public void setNewCars(List<CarDO> newCars) {
		this.newCars = newCars;
	}

	public List<CarDO> getNewCars() {

		return this.newCars;

	}

	public CarDO findCar(String carName) {
		
		return persistenceManager.findCar(carName);
	}
	
	public MemberEntity findMember(String memberName) {
		
		return persistenceManager.findMember(memberName);
	}
	
	public void writeToFile(MultipartFile file) {
		
		persistenceManager.writeToFile(file);
		
	}
}
