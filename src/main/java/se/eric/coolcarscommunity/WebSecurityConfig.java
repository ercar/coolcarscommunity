package se.eric.coolcarscommunity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;

import se.eric.coolcarscommunity.logic.domainobjects.MemberDO;
import se.eric.coolcarscommunity.persistence.PersistenceManager;

@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	PersistenceManager persistenceManager;
	
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/", "/style/**", "/public/**", "/carImages/**", "/memberAvatars/**", "/carprofile/**", "/header", "/footer").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/public/login")
                .permitAll()
                .and()
            .logout()
                .permitAll();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        
    	for( MemberDO currentMember : persistenceManager.getAllMembers()) {
    	
    	auth
            .inMemoryAuthentication()
                .withUser(currentMember.getMemberName()).password(currentMember.getMemberPassword()).roles("USER");
    	
    	}
    }
}