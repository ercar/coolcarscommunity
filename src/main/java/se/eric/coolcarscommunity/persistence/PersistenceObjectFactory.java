package se.eric.coolcarscommunity.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import se.eric.coolcarscommunity.logic.domainobjects.CarDO;
import se.eric.coolcarscommunity.logic.domainobjects.MemberDO;
import se.eric.coolcarscommunity.logic.domainobjects.NewsDO;
import se.eric.coolcarscommunity.persistence.database.CarEntity;
import se.eric.coolcarscommunity.persistence.database.MemberEntity;
import se.eric.coolcarscommunity.persistence.database.NewsEntity;

@Component
public class PersistenceObjectFactory {

	public CarDO createCarDO(CarEntity carEntity) {

		if (carEntity == null) {
			return null;
		}

		final CarDO carDO = new CarDO(carEntity.getpKey(), carEntity.getCarName(),
				carEntity.getCarImage(), carEntity.getCarBrand(),
				carEntity.getCarColor(), carEntity.getCarType(),
				carEntity.getCarDescription(), carEntity.getCarRating(),
				carEntity.getCarVotes(), carEntity.getCreateDate(), carEntity.getMemberEntity());

		return carDO;

	}

	public List<CarDO> createCarDOList(Collection<CarEntity> carsEntities) {

		final List<CarDO> result = new ArrayList<CarDO>();

		for (final CarEntity currentCarEntity : carsEntities) {
			if (currentCarEntity != null) {
				result.add(createCarDO(currentCarEntity));
			}
		}
		return result;
	}

	public MemberDO createMemberDO(MemberEntity memberEntity) {

		if (memberEntity == null) {
			return null;
		}

		final MemberDO memberDO = new MemberDO(memberEntity.getLastLogin(),
				memberEntity.getMemberActive(), memberEntity.getMemberAvatar(),
				memberEntity.getMemberIp(), memberEntity.getMemberLevel(),
				memberEntity.getMemberName(), memberEntity.getMemberPassword(),
				memberEntity.getMemberMail(), memberEntity.getMemberAbout());

		return memberDO;

	}

	public List<MemberDO> createMemberDOList(
			Collection<MemberEntity> memberEntities) {

		final List<MemberDO> result = new ArrayList<MemberDO>();

		for (final MemberEntity currentMemberEntity : memberEntities) {
			if (currentMemberEntity != null) {
				result.add(createMemberDO(currentMemberEntity));
			}
		}
		return result;
	}

	public MemberEntity createMemberEntity(MemberDO memberDO) {

		final MemberEntity memberEntity = new MemberEntity(memberDO.getLastLogin(), memberDO.getMemberActive(),
				memberDO.getMemberAvatar(), memberDO.getMemberIp(), memberDO.getMemberLevel(),
				memberDO.getMemberName(), memberDO.getMemberPassword(), memberDO.getMemberMail(),
				memberDO.getMemberAbout());

		return memberEntity;
	}

	public NewsDO createNewsDO(NewsEntity newsEntity) {

		if (newsEntity == null) {
			return null;
		}

		final NewsDO newsDO = new NewsDO(newsEntity.getNewsActive(),
				newsEntity.getNewsContent(), newsEntity.getNewsType(),
				newsEntity.getMember());

		return newsDO;

	}

	public List<NewsDO> createNewsDOList(Collection<NewsEntity> newsEntities) {

		final List<NewsDO> result = new ArrayList<NewsDO>();

		for (final NewsEntity currentNewsEntity : newsEntities) {
			if (currentNewsEntity != null) {
				result.add(createNewsDO(currentNewsEntity));
			}
		}
		return result;
	}

}