package se.eric.coolcarscommunity.persistence.fileupload;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class ImageUpload {

	public void writeToFile(MultipartFile file) {
		
		if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(
                        		new File("C:/carImages/" +file.getOriginalFilename())));
                stream.write(bytes);
                stream.close();
	
            } catch (Exception e) {
            	e.printStackTrace();
                System.out.println("You failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
            }
        } else {
            System.out.println("You failed to upload " + file.getOriginalFilename() + " because the file was empty.");
        }
}
}