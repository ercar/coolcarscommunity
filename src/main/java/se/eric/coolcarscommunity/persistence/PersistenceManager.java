package se.eric.coolcarscommunity.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import se.eric.coolcarscommunity.logic.domainobjects.CarDO;
import se.eric.coolcarscommunity.logic.domainobjects.MemberDO;
import se.eric.coolcarscommunity.logic.domainobjects.NewsDO;
import se.eric.coolcarscommunity.persistence.database.CarEntity;
import se.eric.coolcarscommunity.persistence.database.CarRepository;
import se.eric.coolcarscommunity.persistence.database.MemberEntity;
import se.eric.coolcarscommunity.persistence.database.MemberRepository;
import se.eric.coolcarscommunity.persistence.database.NewsEntity;
import se.eric.coolcarscommunity.persistence.database.NewsRepository;
import se.eric.coolcarscommunity.persistence.fileupload.ImageUpload;

@Component
public class PersistenceManager {

	@Autowired(required = true)
	CarRepository carRepo;

	@Autowired(required = true)
	MemberRepository memberRepo;

	@Autowired(required = true)
	NewsRepository newsRepo;

	@Autowired
	JdbcTemplate jdbc;

	@Autowired
	PersistenceObjectFactory objectfactory;
	
	@Autowired
	ImageUpload imageUpload;

	public PersistenceManager() {
	}

	public List<CarDO> getAllCars() {

		final List<CarEntity> dbResult = (List<CarEntity>) carRepo.findAll();
		final List<CarDO> result = this.objectfactory.createCarDOList(dbResult);

		return result;

	}

	public void SaveCarEntity(CarDO carDO) {

		CarEntity carEntity = new CarEntity(carDO.getCarName(), carDO.getCarImage(),
				carDO.getCarBrand(),carDO.getCarColor(), carDO.getCarType(),
				carDO.getCarDescription(), carDO.getMember());

		carRepo.save(carEntity);
	}
	
	

	public List<CarDO> getTopFiveCars() {

		String sql = "SELECT * FROM car ORDER BY car_rating DESC LIMIT 5";

		List<CarEntity> dbResult = new ArrayList<CarEntity>();
		dbResult = jdbc.query(sql, new BeanPropertyRowMapper(CarEntity.class));

		List<CarDO> result = this.objectfactory.createCarDOList(dbResult);

		result = addMembersToJDBCFromJPA(result);

		return result;
	}

	public List<CarDO> addMembersToJDBCFromJPA(List<CarDO> carDOList) {

		for (CarDO currentCarDO : carDOList) {

			CarEntity carReplica = carRepo.findOne(currentCarDO.getpKey());

			MemberEntity memberToAdd = carReplica.getMemberEntity();

			currentCarDO.setMember(memberToAdd);
		}

		return carDOList;
	}

	public List<CarDO> getNewCars() {

		String sql = "SELECT * FROM car ORDER BY create_date DESC LIMIT 3";

		List<CarEntity> dbResult = new ArrayList<CarEntity>();
		dbResult = jdbc.query(sql, new BeanPropertyRowMapper(CarEntity.class));

		List<CarDO> result = this.objectfactory.createCarDOList(dbResult);

		result = addMembersToJDBCFromJPA(result);

		return result;
	}

	public List<MemberDO> getAllMembers() {

		final List<MemberEntity> dbResult = (List<MemberEntity>) memberRepo
				.findAll();
		final List<MemberDO> result = this.objectfactory
				.createMemberDOList(dbResult);

		return result;

	}

	public void CreateMember(MemberDO memberDO) {

		MemberEntity memberEntity = new MemberEntity(memberDO.getLastLogin(),
				memberDO.getMemberActive(), memberDO.getMemberAvatar(),
				memberDO.getMemberIp(), memberDO.getMemberLevel(),
				memberDO.getMemberName(), memberDO.getMemberPassword(),
				memberDO.getMemberMail(), memberDO.getMemberAbout());

		memberRepo.save(memberEntity);
	}

	public List<NewsDO> getAllNews() {

		final List<NewsEntity> dbResult = (List<NewsEntity>) newsRepo.findAll();
		final List<NewsDO> result = this.objectfactory
				.createNewsDOList(dbResult);

		return result;

	}

	public void CreateNews(NewsDO newsDO) {

		NewsEntity newsEntity = new NewsEntity(newsDO.getNewsActive(),
				newsDO.getNewsContent(), newsDO.getNewsType(),
				newsDO.getMember());

		newsRepo.save(newsEntity);
	}

	public CarDO findCar(String carName) {
		
		String sql = "SELECT * FROM car WHERE car_name = '" +carName +"'";

		List<CarEntity> dbResult = new ArrayList<CarEntity>();
		dbResult = jdbc.query(sql, new BeanPropertyRowMapper(CarEntity.class));

		List<CarDO> result = this.objectfactory.createCarDOList(dbResult);

		result = addMembersToJDBCFromJPA(result);
		
		return result.get(0);
	}
	
	public MemberEntity findMember(String memberName) {
		
		String sql = "SELECT * FROM member WHERE member_name = '" +memberName +"'";

		List<MemberEntity> dbResult = new ArrayList<MemberEntity>();
		dbResult = jdbc.query(sql, new BeanPropertyRowMapper(MemberEntity.class));
		
		return dbResult.get(0);
		
	}
	
	
	public void writeToFile(MultipartFile file) {
		
		this.imageUpload.writeToFile(file);
		
	}

}
