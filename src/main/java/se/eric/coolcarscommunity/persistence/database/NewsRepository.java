package se.eric.coolcarscommunity.persistence.database;

import org.springframework.data.repository.CrudRepository;

public interface NewsRepository extends CrudRepository<NewsEntity, Integer> {
	
	
	
}
