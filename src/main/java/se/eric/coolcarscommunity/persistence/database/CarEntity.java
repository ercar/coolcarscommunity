package se.eric.coolcarscommunity.persistence.database;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "car")
public class CarEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "p_key", unique = true, nullable = false)
	private int pKey;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", nullable = false)
	protected Date createDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date", nullable = false)
	protected Date updateDate;

	@Column(name = "car_name")
	private String carName;

	@Column(name = "car_image")
	private String carImage;

	@Column(name = "car_brand")
	private String carBrand;

	@Column(name = "car_color")
	private String carColor;

	@Column(name = "car_type")
	private String carType;

	@Column(name = "car_description")
	private String carDescription;

	@Column(name = "car_rating")
	private int carRating;

	@Column(name = "car_votes")
	private int carVotes;

	@ManyToOne
	@JoinColumn(name = "member")
	private MemberEntity memberEntity;

	public CarEntity() {

	}

	public CarEntity(int pKey, String carName, String carImage,
			String carBrand, String carColor, String carType,
			String carDescription, int carRating, int carVotes,
			MemberEntity memberEntity) {
		super();
		this.pKey = pKey;
		this.carName = carName;
		this.carImage = carImage;
		this.carBrand = carBrand;
		this.carColor = carColor;
		this.carType = carType;
		this.carDescription = carDescription;
		this.carRating = carRating;
		this.carVotes = carVotes;
		this.memberEntity = memberEntity;
	}
	
	

	public CarEntity(String carName, String carImage, String carBrand,
			String carColor, String carType, String carDescription,
			MemberEntity memberEntity) {
		super();
		this.carName = carName;
		this.carImage = carImage;
		this.carBrand = carBrand;
		this.carColor = carColor;
		this.carType = carType;
		this.carDescription = carDescription;
		this.memberEntity = memberEntity;
	}

	@PrePersist
	public void onPersist() {
		final Date now = new Date();
		setCreateDate(now);
		setUpdateDate(now);
	}

	@PreUpdate
	public void onUpdate() {
		final Date now = new Date();
		setUpdateDate(now);
	}

	public int getpKey() {
		return pKey;
	}

	public void setpKey(int pKey) {
		this.pKey = pKey;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getCarImage() {
		return carImage;
	}

	public void setCarImage(String carImage) {
		this.carImage = carImage;
	}

	public String getCarBrand() {
		return carBrand;
	}

	public void setCarBrand(String carBrand) {
		this.carBrand = carBrand;
	}

	public String getCarColor() {
		return carColor;
	}

	public void setCarColor(String carColor) {
		this.carColor = carColor;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getCarDescription() {
		return carDescription;
	}

	public void setCarDescription(String carDescription) {
		this.carDescription = carDescription;
	}

	public int getCarRating() {
		return carRating;
	}

	public void setCarRating(int carRating) {
		this.carRating = carRating;
	}

	public int getCarVotes() {
		return carVotes;
	}

	public void setCarVotes(int carVotes) {
		this.carVotes = carVotes;
	}

	public MemberEntity getMemberEntity() {
		return memberEntity;
	}

	public void setMemberEntity(MemberEntity memberEntity) {
		this.memberEntity = memberEntity;
	}

}
