package se.eric.coolcarscommunity.persistence.database;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity(name = "member")
public class MemberEntity extends EntityBase {

	@Column(name = "last_login")
	private Date lastLogin;

	@Column(name = "member_active")
	private String memberActive;

	@Column(name = "member_avatar")
	private String memberAvatar;

	@Column(name = "member_ip")
	private String memberIp;

	@Column(name = "member_level")
	private String memberLevel;

	@Column(name = "member_name")
	private String memberName;

	@Column(name = "member_password")
	private String memberPassword;

	@Column(name = "member_mail")
	private String memberMail;

	@Column(name = "member_about")
	private String memberAbout;

	// bi-directional many-to-one association to Seat
	@OneToMany(mappedBy = "memberEntity")
	private List<CarEntity> carEntities;

	public MemberEntity() {

	}

	public MemberEntity(Date lastLogin, String memberActive,
			String memberAvatar, String memberIp, String memberLevel,
			String memberName, String memberPassword, String memberMail,
			String memberAbout) {
		super();
		this.lastLogin = lastLogin;
		this.memberActive = memberActive;
		this.memberAvatar = memberAvatar;
		this.memberIp = memberIp;
		this.memberLevel = memberLevel;
		this.memberName = memberName;
		this.memberPassword = memberPassword;
		this.memberMail = memberMail;
		this.memberAbout = memberAbout;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getMemberActive() {
		return memberActive;
	}

	public void setMemberActive(String memberActive) {
		this.memberActive = memberActive;
	}

	public String getMemberAvatar() {
		return memberAvatar;
	}

	public void setMemberAvatar(String memberAvatar) {
		this.memberAvatar = memberAvatar;
	}

	public String getMemberIp() {
		return memberIp;
	}

	public void setMemberIp(String memberIp) {
		this.memberIp = memberIp;
	}

	public String getMemberLevel() {
		return memberLevel;
	}

	public void setMemberLevel(String memberLevel) {
		this.memberLevel = memberLevel;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberPassword() {
		return memberPassword;
	}

	public void setMemberPassword(String memberPassword) {
		this.memberPassword = memberPassword;
	}

	public String getMemberMail() {
		return memberMail;
	}

	public void setMemberMail(String memberMail) {
		this.memberMail = memberMail;
	}

	public String getMemberAbout() {
		return memberAbout;
	}

	public void setMemberAbout(String memberAbout) {
		this.memberAbout = memberAbout;
	}

}
