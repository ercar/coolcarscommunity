package se.eric.coolcarscommunity.persistence.database;

import org.springframework.data.repository.CrudRepository;

public interface CarRepository extends CrudRepository<CarEntity, Integer> {
	
}
