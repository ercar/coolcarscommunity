package se.eric.coolcarscommunity.persistence.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "news")
public class NewsEntity extends EntityBase {


	@Column (name = "news_active")
	private String newsActive;
	
	@Column (name = "news_content")
	private String newsContent;
	
	@Column (name = "news_type")
	private String newsType;
	
	@ManyToOne
	@JoinColumn(name = "member")
	private MemberEntity member;
	
	
	public NewsEntity () {
		
	}


	public NewsEntity(String newsActive, String newsContent, String newsType,
			MemberEntity member) {
		super();
		this.newsActive = newsActive;
		this.newsContent = newsContent;
		this.newsType = newsType;
		this.member = member;
	}


	public String getNewsActive() {
		return newsActive;
	}


	public void setNewsActive(String newsActive) {
		this.newsActive = newsActive;
	}


	public String getNewsContent() {
		return newsContent;
	}


	public void setNewsContent(String newsContent) {
		this.newsContent = newsContent;
	}


	public String getNewsType() {
		return newsType;
	}


	public void setNewsType(String newsType) {
		this.newsType = newsType;
	}


	public MemberEntity getMember() {
		return member;
	}


	public void setMember(MemberEntity member) {
		this.member = member;
	}
	
	
}
