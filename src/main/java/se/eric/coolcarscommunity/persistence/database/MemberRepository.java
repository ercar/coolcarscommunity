package se.eric.coolcarscommunity.persistence.database;

import org.springframework.data.repository.CrudRepository;

public interface MemberRepository extends CrudRepository<MemberEntity, Integer> {
	
	
	
}