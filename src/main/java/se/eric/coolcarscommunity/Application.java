package se.eric.coolcarscommunity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class Application extends WebMvcConfigurerAdapter {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/public/videos").setViewName("public/videos");
		registry.addViewController("/public/about").setViewName("public/about");
		registry.addViewController("/public/contact").setViewName("public/contact");
		registry.addViewController("/public/memberprofile").setViewName("public/memberprofile");
		registry.addViewController("/public/login").setViewName("public/login");
		registry.addViewController("/header").setViewName("header");
		registry.addViewController("/footer").setViewName("footer");
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/carImages/**")
				.addResourceLocations("file:///C:/carImages/")
				.setCachePeriod(0);

		registry.addResourceHandler("/memberAvatars/**")
				.addResourceLocations("file:///C:/memberAvatars/")
				.setCachePeriod(0);
	}

}
