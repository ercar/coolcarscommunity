package se.eric.coolcarscommunity.presentation;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.PathVariable;

import se.eric.coolcarscommunity.logic.CarHandlingService;
import se.eric.coolcarscommunity.logic.domainobjects.CarDO;
import se.eric.coolcarscommunity.persistence.database.MemberEntity;

@Controller
public class CarController {

	@Autowired
	CarHandlingService carHandlingService;

	@RequestMapping(value = "/public/index", method = RequestMethod.GET)
	public ModelAndView index() {

		System.out.println("index() kallas");

		ModelAndView model = new ModelAndView();

		return model;
	}

	@RequestMapping(value = "/public/carlist", method = RequestMethod.GET)
	public ModelAndView carlist() {

		System.out.println("carlist() kallas");

		ModelAndView model = new ModelAndView();

		return model;
	}

	@RequestMapping(value = "/carprofile/{selectedCar}")
	public ModelAndView carprofile(@PathVariable Map<String, String> pathVars) {

		CarDO carDO = carHandlingService.findCar(pathVars.get("selectedCar"));

		ModelAndView model = new ModelAndView("carprofile");

		model.addObject("car", carDO);

		return model;
	}

	@RequestMapping(value = "/secure/carfactory", method=RequestMethod.GET)
	public ModelAndView carfactory() {

		System.out.println("/secure/carfactory kallas");

		ModelAndView model = new ModelAndView();

		return model;

	}

	@RequestMapping(value = "/secure/carfactory", method=RequestMethod.POST)
	public ModelAndView carfactory(
			@RequestParam("member") String memberName,
			@RequestParam("file") MultipartFile file,
			@RequestParam("carName") String carName,
			@RequestParam("carBrand") String carBrand,
			@RequestParam("carColor") String carColor,
			@RequestParam("carType") String carType,
			@RequestParam("carDescription") String carDescription) {

		System.out.println("/secure/carfactory inne i post-metod");
		
		ModelAndView model = new ModelAndView();

		carHandlingService.writeToFile(file);
		
		MemberEntity memberToSet = carHandlingService.findMember(memberName);

		CarDO carDO = new CarDO();
		carDO.setMember(memberToSet);
		carDO.setCarImage("carImages/" + file.getOriginalFilename());
		carDO.setCarName(carName);
		carDO.setCarColor(carColor);
		carDO.setCarBrand(carBrand);
		carDO.setCarType(carType);
		carDO.setCarDescription(carDescription);

		carHandlingService.SaveCar(carDO);

		// laddar om alla listor igen för att få med den nya bilen
		carHandlingService.createNewCars();
		carHandlingService.createTopFiveCars();

		return model;
	}

	@RequestMapping(value = "/public/test", method = RequestMethod.GET)
	public ModelAndView test() {

		System.out.println("test() kallas");

		ModelAndView model = new ModelAndView();

		return model;
	}

}
