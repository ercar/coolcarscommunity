
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for cars_db
CREATE DATABASE IF NOT EXISTS `cars_db` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci */;
USE `cars_db`;


-- Dumping structure for table cars_db.car
CREATE TABLE IF NOT EXISTS `car` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `car_name` varchar(200) COLLATE utf8_swedish_ci NOT NULL,
  `car_image` varchar(200) COLLATE utf8_swedish_ci NOT NULL,
  `car_brand` varchar(200) COLLATE utf8_swedish_ci DEFAULT NULL,
  `car_color` varchar(200) COLLATE utf8_swedish_ci DEFAULT NULL,
  `car_type` varchar(200) COLLATE utf8_swedish_ci NOT NULL,
  `car_description` varchar(900) COLLATE utf8_swedish_ci DEFAULT NULL,
  `car_rating` int(11) DEFAULT NULL,
  `car_votes` int(11) DEFAULT NULL,
  `member` int(11) DEFAULT NULL,
  PRIMARY KEY (`p_key`),
  KEY `FK_user_car` (`member`),
  CONSTRAINT `FK_user_car` FOREIGN KEY (`member`) REFERENCES `member` (`p_key`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- Dumping data for table cars_db.car: ~13 rows (approximately)
DELETE FROM `car`;
/*!40000 ALTER TABLE `car` DISABLE KEYS */;
INSERT INTO `car` (`p_key`, `create_date`, `update_date`, `car_name`, `car_image`, `car_brand`, `car_color`, `car_type`, `car_description`, `car_rating`, `car_votes`, `member`) VALUES
	(1, '2015-09-14 18:16:28', '2015-09-18 10:59:37', 'SpeedDemon', 'carImages/car1.jpg', 'Toyota', 'Red', 'Race', 'Dont even try to catch up', 73, 5, 86),
	(2, '2015-09-14 18:17:38', '2015-09-18 11:00:57', 'IceBreaker', 'carImages/car2.jpg', 'Custom', 'White', 'Off-road', 'Get in line and get behind', 53, 3, 87),
	(3, '2015-09-14 18:21:10', '2015-09-18 11:01:00', 'DeathWagon', 'carImages/car3.jpg', 'Custom', 'Black', 'Off-road', 'So much crap', 42, 1, 88),
	(4, '2015-09-14 18:24:30', '2015-09-18 11:01:03', 'DukeNukem', 'carImages/car4.jpg', 'Custom', 'Red', 'Race', 'Hail to the king', 92, 6, 86),
	(5, '2015-09-14 18:27:18', '2015-09-18 11:01:06', 'BigMonster', 'carImages/car5.jpg', 'Custom', 'Red', 'Off-road', 'Run or get overrunned', 86, 10, 87),
	(6, '2015-09-14 18:28:57', '2015-09-18 11:01:09', 'UltraNormal', 'carImages/car6.jpg', 'Custom', 'White', 'City', 'Dont mind me', 95, 16, 88),
	(7, '2015-09-14 18:30:08', '2015-09-18 11:01:11', 'SuperiorArmor', 'carImages/car7.jpg', 'Custom', 'Silver', 'City', 'Cold hard steel ', 22, 5, 86),
	(8, '2015-09-14 18:31:06', '2015-09-18 11:01:14', 'EightBall', 'carImages/car8.jpg', 'Custom', 'Black', 'Pimp', 'Only nine roads to nirvana', 56, 12, 87),
	(9, '2015-09-14 18:35:12', '2015-09-18 11:01:17', 'CowBar', 'carImages/car9.jpg', 'Custom', 'Orange', 'Off-road', 'It will be bumpy', 88, 34, 88),
	(10, '2015-09-14 18:41:24', '2015-09-18 11:01:20', 'JackyJoe', 'carImages/car10.jpg', 'Custom', 'Orange', 'Off-road', 'Just doing my job', 40, 11, 86),
	(17, '2015-09-19 16:34:37', '2015-09-19 16:34:37', 'BumbleBee', 'carImages/car56.JPG', 'AutoBot', 'yellow', 'Transformer', '...', 0, 0, 86),
	(21, '2015-09-19 23:38:41', '2015-09-19 23:41:28', 'KnightRider', 'carImages/car40.JPG', 'Custom', 'black', 'Pimp', 'I am the law', 89, 54, 87),
	(22, '2015-09-21 10:05:24', '2015-09-21 10:05:24', 'SargentJeep', 'carImages/car54.JPG', 'Custom', 'green', 'rally', 'I have not failed', 0, 0, 86),
	(23, '2015-09-22 09:44:35', '2015-09-22 09:44:35', 'Kalle', 'carImages/car49.JPG', 'Custom', 'black', 'race', 'safsadfdsfds dsf dsfsdfdsf d', 0, 0, 86);
/*!40000 ALTER TABLE `car` ENABLE KEYS */;


-- Dumping structure for table cars_db.member
CREATE TABLE IF NOT EXISTS `member` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_login` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `member_active` varchar(50) COLLATE utf8_swedish_ci DEFAULT NULL,
  `member_avatar` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `member_ip` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `member_level` varchar(50) COLLATE utf8_swedish_ci DEFAULT 'member',
  `member_name` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `member_password` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `member_mail` varchar(250) COLLATE utf8_swedish_ci DEFAULT NULL,
  `member_about` varchar(500) COLLATE utf8_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`p_key`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- Dumping data for table cars_db.member: ~2 rows (approximately)
DELETE FROM `member`;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` (`p_key`, `create_date`, `update_date`, `last_login`, `member_active`, `member_avatar`, `member_ip`, `member_level`, `member_name`, `member_password`, `member_mail`, `member_about`) VALUES
	(86, '2015-09-18 08:47:19', '2015-09-18 08:47:19', '2015-09-18 08:47:19', 'Y', NULL, '666666', 'member', 'mrPink', 'password', 'pink.it@yes.com', 'Uh-uh I don\'t tip'),
	(87, '2015-09-18 08:47:58', '2015-09-18 08:51:00', '2015-09-18 08:51:00', 'Y', NULL, '777777', 'member', 'mrBlonde', 'password', 'blondbomb.it@yes.com', 'Was that as good for you as it was for me? '),
	(88, '2015-09-18 08:52:21', '2015-09-18 08:52:21', '2015-09-18 08:52:21', 'Y', NULL, '888888', 'member', 'mrWhite', 'password', 'whitey.it@yes.com', 'Bam. Bam. Bam. Bam. ');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;


-- Dumping structure for table cars_db.news
CREATE TABLE IF NOT EXISTS `news` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `news_active` varchar(50) COLLATE utf8_swedish_ci DEFAULT NULL,
  `news_content` varchar(900) COLLATE utf8_swedish_ci DEFAULT NULL,
  `news_type` varchar(50) COLLATE utf8_swedish_ci DEFAULT NULL,
  `member` int(11) DEFAULT NULL,
  PRIMARY KEY (`p_key`),
  KEY `FK_member_news` (`member`),
  CONSTRAINT `FK_member_news` FOREIGN KEY (`member`) REFERENCES `member` (`p_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- Dumping data for table cars_db.news: ~0 rows (approximately)
DELETE FROM `news`;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
